                  
                  
                    
TUTUM KIDS: APRENDIZAJE INFANTIL SOBRE LA PREVENCIÓN DE ENFERMEDADES
------------

Proposito
------------



-	El propósito de este proyecto en dar a conocer una guía clara y práctica atreves de interacciones,
prevenciones, seguro a prevenir sobre el covid-19.En china existieron 2135 casos pediátricos
relacionados con covid-19 notificados al centro chino para el control y prevención de
enfermedades del 16 de enero del 2020, al 8 de febrero del 2020.


Arquitectura de la App
------------


![](https://emilycauja.000webhostapp.com/arquitectura.png)
El usuario se conecta desde su dispositivo móvil
-	Realiza la petición al SDK de Android mediante alguna interacción con la aplicación
-	El SDK se comunica con el Core de Unity donde esta todas las dependencias y configuraciones
de nuestra aplicación.
-	Unity se comunica con nuestra BD en la nube al momento de realizar una transacción.
-	Unity devuelvo esta transacción para que el SDK la procese.
-	Se devuelve al dispositivo la respuesta procesada del SDK para visualizarlo en el dispositivo.


Diseño de Pruebas
------------


-	Al probar el juego podemos comprobar que cuenta con un excelente funcionamiento y de fácil
uso, como podemos ver en las siguientes pantallas el juego cumple con los objetivos planteados y
con la necesidad encuestada. El juego cuenta con: Menú, submenú, opciones de audio, opciones
de gráfico y los distintos juegos que ayudaran al niño evitar el contagio.
EL Menú cuentas con tres botone que permitirá al jugador iniciar el juego, configurar las
opciones o salir.

![](https://emilycauja.000webhostapp.com/menu.png)

-	Estas son las opciones para configurar los gráficos y el audio de acuerdo al gusto del usuario

![](https://emilycauja.000webhostapp.com/configuracion.png)


-	En el Submenú, hay botones que se llaman, Virus Crush, Aprender y Ball Virus esa sección
contará con juegos interesantes que ayuden al niño sobre todo en Aprender cumplir con el lavado
de manos, uso de gel, distanciamiento social etc.

![](https://emilycauja.000webhostapp.com/submenu.png)